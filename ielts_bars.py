import random
import pygal
import snakemd
import pypandoc
from pygal.style import Style

# Number Generator


def randos():
    """function for creating 10 random integers in a list"""
    nums = []
    for num in range(3):
        """Adds 10 random integers to list"""
        newnum = random.randint(1, 10) * random.random()
        nums.append(round(newnum, 2))
    return nums


# LOOP TO MAKE LOTS OF THINGS

for chart in range(1, 25):

    # Company Generator

    companies = [
        {"x": "Annual Vehicle Sales", "a": "Cars", "b": "Trucks", "c": "Motorcycles"},
        {
            "x": "Annual Toy Sales",
            "a": "Action Figures",
            "b": "Dolls",
            "c": "Video Games",
        },
        {
            "x": "Annual Computer Equipment Sales",
            "a": "Webcams",
            "b": "Keyboards",
            "c": "Mice",
        },
        {
            "x": "Annual Men's Clothing Sales",
            "a": "Trousers",
            "b": "Shirts",
            "c": "Shoes",
        },
        {
            "x": "Annual Women's Clothing Sales",
            "a": "Dresses",
            "b": "Blouses",
            "c": "Shoes",
        },
        {
            "x": "Annual Book Sales",
            "a": "Fiction",
            "b": "Non-Fiction",
            "c": "Personal Development",
        },
        {"x": "Annual Furniture Sales", "a": "Shelves", "b": "Couches", "c": "Beds"},
    ]

    random_retailer = random.choice(companies)
    company_type = random_retailer["x"]
    item_1 = random_retailer["a"]
    item_2 = random_retailer["b"]
    item_3 = random_retailer["c"]

    # Chart Maker

    trend = pygal.Bar(
        width=1600,
        height=800,
        show_legend=True,
        range=(0, 10),
        truncate_legend=25,
        print_values=True,
    )
    trend.style = Style(
        font_family="FreeSans",
        title_font_size=20,
        label_font_size=20,
        major_label_font_size=20,
        legend_font_size=20,
        stroke_width="3",
        dots_size="10",
    )
    trend.title = company_type + " (2024)"
    trend.y_title = "Units sold (thousands)"
    trend.x_labels = [item_1, item_2, item_3]
    trend.add("Ages 25 to 40", randos())
    trend.add("Ages 41 and older", randos())
    trend.render_to_png("test.png")

    # Create MD

    basename = "Bars_" + str(chart)

    doc_title = company_type.lower()

    doc = snakemd.new_doc()
    doc.add_heading("IELTS Writing Task 1")
    doc.add_paragraph("You should spend about 20 minutes on this task.")
    doc.add_paragraph(
        "**The bar chart shows the " + doc_title + " of a company by age group.**"
    )
    doc.add_paragraph(
        "**Summarize the information by selecting and reporting the main features and make comparisons where relevant.**"
    )
    doc.add_paragraph("*Write at least 150 words*")
    doc.add_horizontal_rule()
    doc.add_paragraph("![Bar Chart](test.png)")
    doc.dump(basename)

    # PDF That Sucker

    input = basename + ".md"
    output = basename + ".pdf"
    pdfd = pypandoc.convert_file(
        input,
        "pdf",
        outputfile=output,
        extra_args=[
            "-V",
            "geometry:margin=1.5cm",
            "-V",
            "mainfont:FreeSans",
            "-V",
            "fontsize:12pt",
            "-V",
            "date-meta:2024",
            "-V",
            "title-meta:IELTS Writing Task 1",
            "--pdf-engine=xelatex",
        ],
    )
