"""
Small Script to Generate Random Pie Charts
Outputs to pdf

Requires: pandoc, random, pygal, snakemd, pypandoc
"""

import random
import pygal
#import snakemd
from snakemd import Document
import pypandoc
from pygal.style import Style

# LOOP TO MAKE LOTS OF THINGS

for chart in range(1, 25):

    # Company Generator

    companies = [
        {
            "x": "Social Network Market Share",
            "a": "Hipper",
            "b": "Connect'ID",
            "c": "MainStreamer",
        },
        {
            "x": "Toy Company Market Share",
            "a": "BRICKO",
            "b": "Cool Wheelz",
            "c": "YayPlay",
        },
        {
            "x": "Computer Company Market Share",
            "a": "EyeBeeM",
            "b": "HiPower",
            "c": "HeyZeus",
        },
        {
            "x": "Clothing Company Market Share",
            "a": "Mercury Sportswear",
            "b": "GreenWear",
            "c": "P&P",
        },
        {
            "x": "Supermarket Market Share",
            "a": "Cool-mart",
            "b": "ShopCo",
            "c": "ZeeMart",
        },
    ]

    random_retailer = random.choice(companies)
    company_type = random_retailer["x"]
    item_1 = random_retailer["a"]
    item_2 = random_retailer["b"]
    item_3 = random_retailer["c"]

    # Chart Maker

    num1 = random.randint(2, 98)
    num2 = random.randint(1, (100 - num1))
    num3 = 100 - num1 - num2

    pie1 = pygal.Pie(
        width=800,
        height=800,
        show_legend=True,
        truncate_legend=25,
        print_values=True,
        legend_at_bottom=True,
    )
    pie1.style = Style(
        font_family="FreeSans",
        title_font_size=20,
        label_font_size=20,
        major_label_font_size=20,
        legend_font_size=20,
        value_font_size=20,
        stroke_width="3",
        dots_size="10",
    )
    pie1.title = company_type + " (2010)\n(in %)"
    pie1.add(item_1, num1)
    pie1.add(item_2, num2)
    pie1.add(item_3, num3)
    pie1.render_to_png("pie1.png")

    num4 = random.randint(2, 98)
    num5 = random.randint(1, (100 - num4))
    num6 = 100 - num5 - num4
    pie2 = pygal.Pie(
        width=800,
        height=800,
        show_legend=True,
        truncate_legend=25,
        print_values=True,
        legend_at_bottom=True,
    )
    pie2.style = Style(
        font_family="FreeSans",
        title_font_size=20,
        label_font_size=20,
        major_label_font_size=20,
        value_font_size=20,
        legend_font_size=20,
        stroke_width="3",
        dots_size="10",
    )
    pie2.title = company_type + " (2020)\n(in %)"
    pie2.add(item_1, num4)
    pie2.add(item_2, num5)
    pie2.add(item_3, num6)
    pie2.render_to_png("pie2.png")

    # Create MD

    BASENAME = "Pies_" + str(chart)

    doc_title = company_type.lower()

    doc = Document()
    doc.add_heading("IELTS Writing Task 1")
    doc.add_paragraph("You should spend about 20 minutes on this task.")
    doc.add_paragraph(
        "**These pie charts show the market shares of three companies in 2010 and 2020.**"
    )
    doc.add_paragraph(
        "**Summarize the information by selecting and reporting the main features and make \
        comparisons where relevant.**"
    )
    doc.add_paragraph("*Write at least 150 words*")
    doc.add_horizontal_rule()
    doc.add_paragraph("![Market Share 2010](pie1.png){ width=45% }")
    doc.add_paragraph("")
    doc.add_paragraph("![Market Share 2020](pie2.png){ width=45% }")
    doc.dump(BASENAME)

    # PDF That Sucker

    INPUT_PDF = BASENAME + ".md"
    OUTPUT_PDF = BASENAME + ".pdf"
    pdfd = pypandoc.convert_file(
        INPUT_PDF,
        "pdf",
        outputfile=OUTPUT_PDF,
        extra_args=[
            "-V",
            "geometry:margin=1.5cm",
            "-V",
            "mainfont:FreeSans",
            "-V",
            "fontsize:11pt",
            "-V",
            "date-meta:2024",
            "-V",
            "title-meta:IELTS Writing Task 1",
            "--pdf-engine=xelatex",
        ],
    )
