"""
Random IELTS Writing Task Generator for Line Charts

Add/remove companies from 'companies' to customise
"""

import random
import pygal
import snakemd
import pypandoc
from pygal.style import Style

# Number Generator
def randos():
    """function for creating 10 random integers in a list"""
    nums = []
    COUNTER = 10
    while COUNTER > 0:
        newnum = random.randint(1, 10) * random.random()
        nums.append(newnum)
        COUNTER -= 1
    return nums


# LOOP TO MAKE LOTS OF THINGS

for chart in range(1, 25):

    # Company Generator

    companies = [
        {"x": "Annual Vehicle Sales", "a": "Cars", "b": "Trucks", "c": "Motorcycles"},
        {
            "x": "Annual Toy Sales",
            "a": "Action Figures",
            "b": "Dolls",
            "c": "Video Games",
        },
        {
            "x": "Annual Computer Equipment Sales",
            "a": "Webcams",
            "b": "Keyboards",
            "c": "Mice",
        },
        {
            "x": "Annual Men's Clothing Sales",
            "a": "Trousers",
            "b": "Shirts",
            "c": "Shoes",
        },
        {
            "x": "Annual Women's Clothing Sales",
            "a": "Dresses",
            "b": "Blouses",
            "c": "Shoes",
        },
        {
            "x": "Annual Book Sales",
            "a": "Fiction",
            "b": "Non-Fiction",
            "c": "Personal Development",
        },
        {
            "x": "Annual Furniture Sales",
            "a": "Shelves",
            "b": "Couches",
            "c": "Beds"},
    ]

    random_retailer = random.choice(companies)
    company_type = random_retailer["x"]
    item_1 = random_retailer["a"]
    item_2 = random_retailer["b"]
    item_3 = random_retailer["c"]

    # Chart Maker

    trend = pygal.Line(
        width=1600, height=800, show_legend=True, range=(0, 10), truncate_legend=25
    )
    trend.style = Style(
        font_family="FreeSans",
        title_font_size=20,
        label_font_size=20,
        major_label_font_size=20,
        legend_font_size=20,
        stroke_width="3",
        dots_size="10",
    )
    trend.title = company_type
    trend.y_title = "Units sold (millions)"
    trend.x_labels = [
        "2010",
        "2011",
        "2012",
        "2013",
        "2014",
        "2015",
        "2016",
        "2017",
        "2018",
        "2019",
    ]
    trend.add(item_1, randos())
    trend.add(item_2, randos())
    trend.add(item_3, randos())
    trend.render_to_png("test.png")

    # Create MD

    BASENAME = "Lines_" + str(chart)

    doc_title = company_type.lower()

    doc = snakemd.new_doc()
    doc.add_heading("IELTS Writing Task 1")
    doc.add_paragraph("You should spend about 20 minutes on this task.")
    doc.add_paragraph(
        "**The line graph shows the "
        + doc_title
        + " of a company over a 10 year period.**"
    )
    doc.add_paragraph(
        "**Summarize the information by selecting and reporting the main features and make comparisons where relevant.**"
    )
    doc.add_paragraph("*Write at least 150 words*")
    doc.add_horizontal_rule()
    doc.add_paragraph("![Line Chart](test.png)")
    doc.dump(BASENAME)

    # PDF That Sucker

    PDF_INPUT = BASENAME + ".md"
    PDF_OUTPUT = BASENAME + ".pdf"
    pdfd = pypandoc.convert_file(
        PDF_INPUT,
        "pdf",
        outputfile=PDF_OUTPUT,
        extra_args=[
            "-V",
            "geometry:margin=1.5cm",
            "-V",
            "mainfont:FreeSans",
            "-V",
            "fontsize:12pt",
            "-V",
            "date-meta:2024",
            "-V",
            "title-meta:IELTS Writing Task 1",
            "--pdf-engine=xelatex",
        ],
    )
