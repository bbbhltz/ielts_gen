# IELTS Chart Generators

Sloppy scripts to make IELTS graphs and charts

## Requirements

* [Pandoc](https://pandoc.org/index.html)
* [Pygal](https://www.pygal.org/en/stable/)
* [pypandoc](https://github.com/NicklasTegner/pypandoc)
* [SnakeMD](https://www.snakemd.io/en/latest/)

## Quick Start

1. Clone it...

```
# git clone https://codeberg.org/bbbhltz/ielts_gen.git
```

2. Set up environment...

```
# cd ielts_gen
# python3 -m venv venv
# source venv/bin/activate
# pip install -r requirements.txt
```

3. Run it...

```
# python3 ielts_bars.py
```

4. Clean up...

```
# deactivate
# pipenv --rm
```

## Examples

*not great, but good enough for practice*

![Bar Chart](bars.png "Example Bar Chart")

![Pie Chart](pies.png "Example Pie Chart")

![Line Graph](lines.png "Example Line Graph")

## TODO

- [ ] Add a few lines to "clean up" and move the output files to their own folders, and delete the cruft
